![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/075956_be1fb9ad_5631341.jpeg "石榴派3-989.jpg")

当我写下 “石榴派核心志愿者通讯” 的时候，我的内心是激动的。刚刚完成 FPGA 专题是星辰大海，内心久未平复的我，捧着昨日完稿打样的 《民族棋大团结》手抄报合集 以及桌面上 已经交付 1 周的 “Shiliu Pi 99 阳光版”，“石榴派真的到发布的时候了！”

> 看着封面，满满的感慨，满满的敬意，淡黄色的底，衬着 Silicon 显微图片的五彩斑斓，最新的 FPGA 的三个可编程方向（软件，硬件，接口）的框图表示的是石榴派的方向和借力。Programable Personal Computer 可编程个人计算机比起一个月前的 石榴核 又精进了。从 OS 发行版的跨平台 ( ARM, RISC-V, X86 ) 配合可编程核心 Silicon 计划，面向个人计算的全栈计算平台已经清晰在目了。而之所以聚焦个人计算，是 石榴派 教育使命的体现，赋能学习者，而非开发者，展示了它的视野。学习者应当拥抱赤子之心的热情，代表了未来的方向，正合青少年为起点的教育场景，在被全栈能力锤炼的青少年一代将成长为终身学习者，带领着人类去实现星辰大海的美好愿景。

PS: MagSi-proteomics beads are magnetic silica beads coated with C4, C8 or C18 alkyl groups, providing a reversed phase (RP) surface chemistry. These beads are an ideal tool for protein and peptide sample concentration, desalting and fractionation, and reduce sample complexity. :） form bing.com

> 另外，出刊团队是 SIGer 学习小组 和 SIGer 编委会，前者是 RV4kids 子刊团队，后者是 科普期刊 主刊，而作为 科普载体的 石榴派 当合力举办，以核心志愿者为名，体现了石榴籽团聚的意义。本想启用 ShiliU 体现核心志愿者的能量，太过分散关注力也是一个资源的浪费。

## [RV4kids 5th FPGA study](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9) 
完成了上边年开源学习的所有 [ISSUES](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5695434_link) 的梳理，

- 完成了[三大板块](https://gitee.com/RV4Kids/RVWeekly/issues/I3YZL9#note_5699153_link)，

  - 完成了 [FPGA](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md) 
  - 就剩下 [石榴派](https://gitee.com/RV4Kids/RVWeekly/issues/I3UEA7#note_5843531_link) 
  - 和 [SIGer](https://gitee.com/flame-ai/siger) 啦。

### [shiliupi](https://gitee.com/shiliupi)

[shiliupi99](https://gitee.com/shiliupi/shiliupi99) 是石榴派的主仓入口，它之下的任务有其历史原因，记录了一些缘起。其中 [picycle](https://gitee.com/shiliupi/shiliupi99/issues/I3RA7Z) 由来已久，要追溯到 对树莓派的模仿。年初以 “[民族棋私有云](https://gitee.com/blesschess/shiliupi)” 方案立足 openEuler 社区时起，就 Fork 了 [sig-raspberry](https://gitee.com/openeuler/raspberrypi) 组为目标，期待在 [openEuler 的树莓派版](https://gitee.com/openeuler/raspberrypi)本基础上构建 民族棋示教的北向应用。当时，[南向北向概念](https://gitee.com/flame-ai/siger/blob/master/%E7%AC%AC10%E6%9C%9F%20%E5%8D%97%E5%BE%81%E5%8C%97%E6%88%98%EF%BC%88%E4%B8%8A12%EF%BC%89.md)形成前，也是 [SIGer 期刊](https://gitee.com/flame-ai/siger/)前身的调研阶段，Fork 了 《开源指北》的分支的分支为 《[欧来指北](https://gitee.com/flame-ai/hello-openEuler)》，现在还会将只与 openEuler 相关的技术贴移入到其中：[GaussDB数据库Linux安装](https://gitee.com/flame-ai/hello-openEuler/issues/I3SJH1) 就是其中之一，整个指北系列是收录在 “[FlameAI](https://gitee.com/flame-ai/) / [欧来指北](https://gitee.com/flame-ai/hello-openEuler)” 下的。对 openEuler 兄弟 SIG 的支持，这里不在一一感谢。如今，专注石榴派的构建，就是对 openEuler 全平台 发行战略的践行：ARM.X86.RISCV。 **[石榴派 X86 镜像 U 盘需求发布！](https://gitee.com/shiliupi/shiliupi99/issues/I3YL6P)**  是最新任务，请持续关注。

- shiliupi / [shiliupi99](https://gitee.com/shiliupi/shiliupi99/issues)

  - #[I3RA7Z](https://gitee.com/shiliupi/shiliupi99/issues/I3RA7Z) picycle 与 PI矩阵的绿色经济的云计算方案
  - #[I3YL6P](https://gitee.com/shiliupi/shiliupi99/issues/I3YL6P) 石榴派 X86 镜像 U 盘需求发布！

民族棋人工智能示教，是石榴派最先明确的北向应用，在 RISCV 石榴派出席 RVCN 中国峰会期间，正式了发布了：[GNUchess](https://gitee.com/shiliupi/gnuchess) + openEuler OS [sig-RISC-V](https://gitee.com/shiliupi/RISC-V) + Rocket Chip Risc-V 垂直 RV 应用的全栈方案，当时罗列了另外的两个 GNU 应用之一就是：[GNOME sudoku](http://)，围绕 GNOME 又收录了两个学习贴：Enable snaps on Debian and install GNOME，红帽 Linux GNOME。

- shiliupi / [gnome-sudoku](https://gitee.com/shiliupi/gnome-sudoku/issues)

  - #[I3U3ZI](https://gitee.com/shiliupi/gnome-sudoku/issues/I3U3ZI) GNOME sudoku
  - #[I3U9RA](https://gitee.com/shiliupi/gnome-sudoku/issues/I3U9RA) Enable snaps on Debian and install GNOME S...
  - #[I3U6NK](https://gitee.com/shiliupi/gnome-sudoku/issues/I3U6NK) RedHat Linux 红帽Linux GNOME使用技巧

民族棋北向应用还有一个重要的支撑就是中间件和编程平台：JAVA 当仁不让，由于 [LuckyLudii](https://gitee.com/blesschess/LuckyLudii) 民族棋中国史的国际伙伴 [LudiiGames](https://gitee.com/blesschess/LuckyLudii/tree/master/ludii.games) 选用的 JAVA 平台，因此，在民族棋全栈示范应用中是标配。Python 中文，得益于 Python 在青少年编程教育中的地位，选择中文是民族风的考量，入选  **python 中文版**  还是 [grasspy380](https://gitee.com/shiliupi/grasspy380/issues/I3HZGV) 优待进一步实践。

- shiliupi / [grasspy380](https://gitee.com/shiliupi/grasspy380/issues) 

  - #[I3HZGV](https://gitee.com/shiliupi/grasspy380/issues/I3HZGV) 是否保留了英文关键字呢？

### [blesschess](https://gitee.com/blesschess/)

"[福星祺福](https://gitee.com/blesschess/luckystar)" 是 blesschess 的中文译名，也是 [袁老师](https://gitee.com/yuandj) 微信头像 福祺 的缘起，最新的民族棋手抄报的封底故事 “[吉祥数独](https://gitee.com/blesschess/luckystar/tree/master/LuckyDoku)” 的八吉祥图腾就来源于 BlessChess 也是火焰棋实验室进入开源社区的第一次亮相的源头。openEuler 社区还为民族棋建立了两个仓：一个就是 openEuler / [blesschess](https://gitee.com/openeuler/blesschess) ，一个就是 AI 引擎 openEuler / [duoyibu-ai](https://gitee.com/openeuler/duoyibu-ai)

- [LuckyLudii](https://gitee.com/blesschess/LuckyLudii/issues)

  - #[I3U3ME](https://gitee.com/blesschess/LuckyLudii/issues/I3U3ME) [General Game Playing](https://gitee.com/blesschess/LuckyLudii/issues/I3U3ME) 通用游戏引擎是一个人工智能学术研究的方向，我们的民族棋引擎就建立在这样的技术上，当前在建的民族棋数据库 1200+，涵盖了世界范围的各类棋盘游戏，抽象出上万个参数，其学术意义包括：通过数学模型的演进，推测文化脉络的发展。

  - #[I3GZLQ](https://gitee.com/blesschess/LuckyLudii/issues/I3GZLQ) [LuckyLuddi IS also a competetion](https://gitee.com/blesschess/LuckyLudii/issues/I3GZLQ) 是一个软广，为我们的国际伙伴打的，今年8月在哥本哈根将举办 [CoG 2021](https://gitee.com/blesschess/LuckyLudii/issues/I3GZLQ#note_4754786_link) 期间将有数十项 人工智能游戏领域的竞赛，包括 DOTA ，LudiiGames 会有专场竞赛 $1000 角逐 GGP 大奖，规则大致是：20个随机的棋盘游戏，跑各参赛队的通用引擎，各游戏排行积分后，总积分排名决定奖项。竞赛只能是北向的一个辅助，不能算目标。

  - #[I3EQ4H](https://gitee.com/blesschess/LuckyLudii/issues/I3EQ4H) [中东跳棋 Alquerque](https://gitee.com/blesschess/LuckyLudii/issues/I3EQ4H) & #[I3EQ4H](https://gitee.com/blesschess/LuckyLudii/issues/I3EQ4H) [鼗鼓](https://gitee.com/blesschess/LuckyLudii/issues/I3EOS0) 则是民族棋数据库的专项游戏文化线索的丰富和翻译，它有两个目标，一个是单个游戏的文化完善，如 Wiki 一样，需要一个众人拾柴的过程，翻译是本地化过程，也是民族棋中国史的主要工作内容，只有全面地掌握国际库中的内容，才能更好地提交中国样本。

    - #[I3CBIH](https://gitee.com/blesschess/LuckyLudii/issues/I3CBIH) [中国棋盘游戏史项目招募志愿者](https://gitee.com/blesschess/LuckyLudii/issues/I3CBIH)，就做了简要的任务说明。

    - #[I3CBIJ](https://gitee.com/blesschess/LuckyLudii/issues/I3CBIJ) [基于 FCscript 的 棋盘游戏 UI 引擎的 单个棋盘游戏的 示教程序开发](https://gitee.com/blesschess/LuckyLudii/issues/I3CBIJ)！则是民族棋示教的成果体现，和民族棋私有云方案一样，我们需要甄别地，有选择地做民族棋示教工作，就需要赋能志愿者进行自己的民族棋示教列表，这样单个棋盘游戏示教，就不需要加载全数据库。

      > 当然开源的民族棋数据库也是民族棋中国史的重要任务，我们可以将示教引擎做更有利于民族棋示教的功能，支持，民族棋子集的批量示教。ps: duoyibuAI - [a framework is the target also an ability ...](https://gitee.com/yuandj/duoyibu-ai/issues/I2D0K1)

- [石榴派](https://gitee.com/blesschess/shiliupi/) fork from openEuler [sig-raspberrypi](https://gitee.com/openeuler/raspberrypi)

  则是最早树莓派学习的痕迹，其中收录了几篇相关仿板的信息，对于超越和模仿的讨论，在中国从来就不缺少，比如 RISCV 社区就有过较早前消息，$15 的 RV 板被超预期的故事。无论怎样，后来者的追赶会永远存在，某一日被颠覆后，推倒重来也不为过，这就是世界本该有的样子。现在的我，对我们的石榴充满信心，水果界的新星已经诞生啦。

  - #[I3Q5CI](https://gitee.com/blesschess/shiliupi/issues/I3Q5CI) 荔枝派Zero 用户指南 http://zero.lichee.pro/
  - #[I3Q4G7](https://gitee.com/blesschess/shiliupi/issues/I3Q4G7) [电脑] 懂LINUX会玩荔枝派Lichee Zero的进来看看

  ## “石榴派” 南北向分析
（此表可绘制成图）下南，上北。

| layers | 1. | 2. | 3... |
|---|---|---|---|
| Games | LuckyStar | LuckyDoku | Alquerque & 鼗鼓 1200+... |
| BlessChess | LuckyLudii | LudiiGames | JAVA FSscript Python 中文 ... |
| Shiliu Pi |  **ARM**  raspberryPi |  **RISC-V**  Shiliu Pi FPGA |  **X86**  Shiliu Pi 99 阳光 |
| SIGer 学习小组 | RV4kids - RVweekly - ICwiki | 一生一芯 3rd: NutShell ... | PLCT: RVOS ... |

- 就最下端的学习小组，可以是最南向 - 人才或者社区，也可以是最北向的教育输出。
- 而后，自下而上是：硬件（石榴派），软件（通用游戏引擎），示教场景（民族棋数据库）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/161413_ab629f7d_5631341.jpeg "2017058416 222.jpg") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/161426_8302ec51_5631341.jpeg "16889126 222.jpg")

- Games & BlessChess : 

  - 民族棋大团结 手抄报（ 飞翔的山鹿 / 鼗鼓棋 ）& 幸运星 LuckyStar
  - 九子仙棋 手抄报（ 百年历史的民族棋，贤者九子棋，幸运星，均变体自它 ）
  - 吉祥数独 LuckyDoku（ 藏棋 "久" 的灵感 - 为他人祺福，结合 民族棋通用游戏引擎 分享 快乐制题法  ）

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/160404_b19cc4ae_5631341.png "阳光副本.png")](https://images.gitee.com/uploads/images/2021/0716/102206_0c07d596_5631341.jpeg) 

- BlessChess ：较早前 石榴派体系，增加了 RV FPGA，以及新学习社区（后面附图）
- Shiliu Pi ：Shiliu Pi 99 阳光 是 X86 体系架构的最新 石榴派发行版。

[![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/154702_18f5f903_5631341.jpeg "XILINX2010ii副本300.jpg")](https://gitee.com/RV4Kids/RVWeekly/blob/master/RV4Kids/RV4kids%205th%20FPGA%20study.md) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/154727_2ba349b1_5631341.jpeg "nutshell副本300.jpg")](https://images.gitee.com/uploads/images/2021/0716/101354_ff29b4b5_5631341.jpeg) [![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/154743_af592169_5631341.jpeg "rvos2副本300.jpg")](https://images.gitee.com/uploads/images/2021/0716/101441_8dbc0f29_5631341.jpeg) 

最南向社区支持：SIGer 学习小组 

- RV4kids - RVweekly - ICwiki 
- 一生一芯 3rd: NutShell ... 
- PLCT: RVOS ... 

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/174401_20610a66_5631341.jpeg "技术漂流.jpg") ![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/174414_92cad156_5631341.jpeg "文化漂流副本.jpg")

- 石榴派 漂流计划 Shiliu Pi (Pomegranate Pi) Drift Plan.

  双漂流，北向文化和南向技术会师于 社区民族棋示教，第一个游戏场景就是吉祥数独，使用石榴派的算力辅助志愿者完成 吉祥数独 题的制作，曰：分享 快乐制题法！一共 9 道标准六宫数独，以 吉祥八宝 图形为题面，伴随题目的求解成功，预示吉祥顺遂的美好愿望，出题的志愿者，可以赋予自己所出题目 特定的意义 - 寄语。以 祺福他人。

  [![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/180517_04349422_5631341.png "屏幕截图.png")](https://gitee.com/shiliupi/shiliupi99/issues/I3YL6P)

  - 技术社区漂流：
    - Siliu Pi 99 Risc-v FPGA, 
    - Siliu Pi 99 RiscV SD card.
    - CNRV 2021 T-Shirt.

  - 民族棋文化漂流：
    - 民族棋大团结 手抄报 99 份
    - 棋盘自封袋 99 枚
    - 志愿者卡 / 吉祥数独 99 张

![输入图片说明](https://images.gitee.com/uploads/images/2021/0715/184912_a0c3821a_5631341.jpeg "luckystones副本.jpg")

- 漂流计划的目标是：收集 333 枚 彩石 和 99 份寄语。由社区共同 制作一副 “福棋”，完成年初之愿。

  - #[I3YL4C](https://gitee.com/blesschess/luckystar/issues/I3YL4C) Building a “Blesschess” Set with Kids All Over the World - 石榴派漂流计划
  - 今还有一愿：祺福“石榴派”早日装备到火种志愿者手中。为万千少年带去欢乐！ 昭告天下 9999s 求见证！
