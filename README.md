# Shiliu Pi 99

![输入图片说明](https://images.gitee.com/uploads/images/2021/0228/035826_1fe0284a_5631341.png "屏幕截图.png")

#### 介绍
"Your complete personal computer, built into a compact keyboard." It's title of Raspberry PI 400. Shiliu PI is a chess computer OS project. Shiliu PI 99 also a pc same as PI400 with the named 99 for best wishtes forever.

#### 缘起
- [blesschess](https://gitee.com/blesschess) / [石榴派](https://gitee.com/blesschess/shiliupi) ： 彼时还只是 (石榴/56)
- forked from [openEuler](https://gitee.com/openeuler) / [raspberrypi](https://gitee.com/openeuler/raspberrypi) ：by @[jianminw](https://gitee.com/jianminw) 社区第一位导师。
- [2020/05/21](https://gitee.com/shiliupi/shiliupi99/issues/I3RA4Z#note_5255117_link) 收到两块 [3B+](https://images.gitee.com/uploads/images/2021/0527/180903_222a5b08_5631341.png) 来自 [openEuler](https://gitee.com/openeuler) [社区](https://gitee.com/openeuler/community/) [TC](https://gitee.com/openeuler/community/tree/master/sig/TC) 委员会的 @[shinwell_hu](https://gitee.com/shinwell_hu)

#### 安装教程

1. 根据设备选择镜像文件

   | 镜像名称 | 下载地址 | 适用设备 |
   |---|---|---|
   | [Raspberry Pi OS](https://www.raspberrypi.org/software/) with desktop and recommended software | Release date: <br>March 4th 2021<br>Kernel version: <br>5.10<br>Size: 2,868MB<br>[Release notes](https://downloads.raspberrypi.org/raspios_full_armhf/release_notes.txt)<br><br>[Download](https://downloads.raspberrypi.org/raspios_full_armhf/images/raspios_full_armhf-2021-03-25/2021-03-04-raspios-buster-armhf-full.zip) | [All Raspberry Pi models](https://www.raspberrypi.org/products/)
   | [Raspberry Pi OS](https://www.raspberrypi.org/software/) with desktop | Release date: <br>March 4th 2021<br>Kernel version: <br>5.10<br>Size: 1,180MB<br><br>[Download](https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2021-05-28/2021-05-07-raspios-buster-armhf.zip) | [All Raspberry Pi models](https://www.raspberrypi.org/products/)
   | openEuler 版:<br>raspios_arm64-2021-05-28<br>openEuler-21.03 kernel<br>bisheng-jdk11<br>LAMP<br>wordpress<br>Phpmyadmin | [下载](https://onedrive.chainsx.cn/?/Linux-Archive/shiliupi/beta-1.0/shiliupi-for-raspberrypi-beta.img.xz) | 3B+ 4B |

2. 下载并设置桌面图片为 [LuckyStar0521.jpg](LuckyStar20210521.jpg)

   - sudo raspi-config (enable SSH first, for raspi-imgs only.)
   - git clone https://gitee.com/shiliupi/shiliupi99

3. git clone https://gitee.com/blesschess/LuckyLudii

   - java -version (need [Java8](https://gitee.com/shiliupi/ubuntu64-rpi/issues/I3MW2Y) above.)
   - java -jar libs/LuckyLudii3.3.jar
   - Load Game from file (Ctrl-F) resources/LuckyStar3.3.lud

4. git clone https://gitee.com/blesschess/shiliupi.git

   - [Build a LAMP Web Server with WordPress](https://projects.raspberrypi.org/en/projects/lamp-web-server-with-wordpress/) ([Wiki](https://gitee.com/blesschess/shiliupi/wikis/?sort_id=4064304))
   - /blesschess/2.html
   - /duoyibu-ai/ngnc/index.html

#### 使用说明

1.  系统概况 

openEuler 镜像
- 使用树莓派官方系统（raspios_arm64-2021-05-28），
- 替换为openEuler内核（openEuler-21.03），
- 预装毕昇jdk，LAMP，wordpress，
- 经过测试可以在树莓派3B和4B上使用。

查看jdk版本：`java --version` 会有所不同。

2.  系统和软件使用用户及密码

- 系统用户名：pi  
Pi用户密码：raspberry

- Mysql用户：root  
Mysql用户root密码：raspberry

- Wordpress用户：pi  
Wordpress用户密码：raspberry

- Wordpress地址：http://openEuler  
（树莓派内可用chromium-browser打开http://127.0.0.1）

- 使用openeuler内核，树莓派桌面环境
Phpmyadmin地址：http://openEuler/phpmyadmin

- 使用mysql账户登陆  
（树莓派内可用chromium-browser打开http://127.0.0.1/phpmyadmin）

3.  检查更新三个相关仓：

- https://gitee.com/blesschess/shiliupi
- https://gitee.com/blesschess/LuckyLudii
- https://gitee.com/shiliupi/shiliupi99

通常在 /home/pi/Download 目录下下载后，ls 结果是三个目录

```
shiliupi LuckyLudii shiliupi99
```

4.  设置 jar 文件默认启动项是 java -jar

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
